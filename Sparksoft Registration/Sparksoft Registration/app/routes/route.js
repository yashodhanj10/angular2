//Set Up	
	var express = require('express');
	var router = express.Router();              

// middleware to use for all requests
	router.use(function(req, res, next) {
	    // do logging
	    res.header("Access-Control-Allow-Origin", "*");
  		res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  		console.log(req.body);
  		console.log('Something is happening.');
	    next(); // make sure we go to the next routes and don't stop here
	});

	// test route to make sure everything is working (accessed at GET http://localhost:8080/api)
	router.get('/', function(req, res) {
	    res.json({ message: 'hooray! welcome to our api!' });   
	});

// Routes
	var bal = require('../modules/buisnesslogic.js');

	//User

		//Get All Users
		router.route('/user')

		.get(function(req, res) {        		
			bal.getAllUser(function(returnValue) {
				res.json(returnValue);
			});

	    })
		
		//Check Email

		router.route('/user/checkemail/:email')

		.get(function(req, res) {        
		    bal.checkEmail(req.params.email, function(returnValue) {
				res.json(returnValue);
			});     
	    })


		//Register User

		router.route('/user/register')

		.post(function(req, res) {                
			bal.registerUser(req.body, function(returnValue) {
				res.json(returnValue);
			});         
	    })

		

		

	 	//Login User

	 	router.route('/user/login')

		.post(function(req, res) {
			console.log("Login Route");
			bal.login(req.body, function(returnValue) {
				res.json(returnValue);
			});    
	    })

	

module.exports = router;
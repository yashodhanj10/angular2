//SpSoft Registration Business logic of APIs




//Database configuration
	var mysql = require('mongojs');
	/*Database configuration 
	------------



	*/

//Encrytion Configuration for encrypting password
	var simplecrypt = require("simplecrypt");
	var opt = {password:"password",salt:"salt"}; 
	var sc = simplecrypt(opt);

//Buissness Logic

	

	//Check Email
	exports.checkEmail = (email,callback) => checkEmail(email,callback);

	function checkEmail(email,callback) {
		try
	        {
		        pool.getConnection(function(err, connection) {
		 		// Use the connection
				  	var query = connection.query( 'User Query to be inserted here ', email, function(err, result) {
				    	connection.release();
					    console.log(query.sql);
					    if(err)
					    {
					    	callback('error');
					    	return;
					    }

					    if(result.length>0)
					    {
					    	callback({ isValid: false }); 
					    }
					    else
					    {
					    	callback({ isValid: true });
					    }
				 	});
				});
		    }
		    catch(err)
		    {
		    	console.log(err);
		    }       
	}

	//Register User
	exports.registerUser = (user,callback) => registerUser(user,callback);

	function registerUser(user, callback) {
		try
	        {	        
		        var unencryptedPassword = user.password;    
		        user.password = sc.encrypt(unencryptedPassword);
		        console.log(user);

		        checkEmail(user.email, function(returnValue) {
					if(returnValue=='error')
					{
						callback(returnValue);
						return;
					}
					else if(!returnValue.isValid)
					{
						callback('error : Username Already Exists');
						return;
					}
					else if(returnValue.isValid)
					{
						pool.getConnection(function(err, connection) {
					 		// Create user in data base
						  	var insertUserQuery = connection.query( 'Register query to be inserted here', user,  function(err, result) {
						    
						    console.log(insertUserQuery.sql);
						    if(err)
						    {	
						    	console.log(err);
						    	callback('error');
						    	return;
						    }

						    //Email for Registration
						    var body =  '<b>Hello '+ user.first_name + ' ' + user.middle_name + ' ' + user.last_name + '</b>' +
						    			'</br><p>      You have been Successfully registered to our database.</p>' +
						    			'<p>Your user name is your email : '+ user.email + '</p>' +
						    			'<p>and your password is : '+ unencryptedPassword + '</p>'+
						    			'</br></br></br></br>'+
						    			'<p>Regards</p>'+ 
						    			'<p>Team SpSoft 🐴</p>';
						    var head = 'SpSoft Registration success.';
						    var mail = require('../modules/mail.js');
							mail.prepareSendEmail(body,head, user.email);

					 		});

					 		var selectUserQuery = connection.query( 'User Query', user.email,  function(err, result) {
							    connection.release();
							    console.log(selectUserQuery.sql);
							    if(err)
							    {	
							    	console.log(err);
							    	callback('error');
							    	return;
							    }
							    callback(result);
							});
						});
					}
				});		       

		        
		    }
		    catch(err)
		    {
		    	console.log(err);
		    }
	}

	

	//Login
	exports.login = (user,callback) => login(user,callback);

	function login(user, callback) {
		try
	    {
	        pool.getConnection(function(err, connection) {
		 		// Use the connection
				var query = connection.query( 'Login query to be inserted here', [user.email,sc.encrypt(user.password)], function(err, result) {
				    connection.release();
				    console.log(query.sql);
				    if(err)
				    {
				    	callback('error');
				    	return;
				    }
				    if(result.length>0)
				    {
				    	callback({ message: 'Login Sucessful!', first_name: result[0].first_name,user_id: result[0].user_id});   
				    }
				    else
				    {
				    	callback({ message: 'invalid Credential' });   
				    }
			 	});
			});
	    }
	    catch(err)
	    {
	    	console.log(err);
	    }
	}

	